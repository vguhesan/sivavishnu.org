** Instructions **

  * Make sure Hugo.exe is in the environmental variable $PATH
  * cd <project_root>

** To run a webserver with latest blog (including drafts): **
<code>
hugo --theme=hugo-bootswatch server --buildDrafts
</code>

** To generate the latest blog: **
<code>
hugo --theme=hugo-bootswatch
</code>

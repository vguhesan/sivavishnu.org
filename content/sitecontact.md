+++
date = "2016-06-04T16:25:58-04:00"
draft = false
title = "Website Manager Details"
layout = "simple-static"
type = "static"
+++

<hr>
<b>This website is managed and maintained by:</b>

<b>Venkatt Guhesan</b></br>
By Email at <a href="&#x6d;&#x61;&#x69;&#108;&#116;&#111;&#x3a;&#x76;&#103;&#x75;&#104;&#x65;&#x73;&#97;&#110;&#64;&#103;&#109;&#x61;&#x69;&#108;&#46;&#99;&#x6f;&#x6d;">&#118;&#x67;&#x75;&#104;&#x65;&#115;&#97;&#110;&#x40;&#x67;&#x6d;&#97;&#105;&#x6c;&#46;&#x63;&#x6f;&#109;</a></br>
Via Phone at +1 443-414-9683 (international charges may apply)</br>
Blog Link: <a href="https://mythinkpond.com/about/" target="_blank">My Personal Blog | தனிப்பட்ட வலைப்பதிவு</a>

<b>Grandson of:</b></br>
Vetaikaranputhur Thiru Meenakshi Sundaram Pillai and Thirumathi Valliamal (fathers side)</br>
Udumalaipetai|Kaniyur Thiru Saravanam Pillai and Thirumathi Thangamal (mothers side)</br>

<b>Parents:</b></br>
Thiru Guhesan and Thirumathi Thilakavathi</br>


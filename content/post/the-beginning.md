+++
date = "2016-06-04T16:25:58-04:00"
draft = false
title = "First Blog | முதல் வலைப்பதிவு"

+++

Welcome to the first post on this website. This site was created on Saturday, June 4th, 2016. I have decided to create a website to document the history, daily practices, festivals and celebration of this temple started by my grandfather and other important folks in Vetaikaranputhur and Kaniyur. This temple <b>Arulmigu Saturagiri Mahalinga Swami Arulmigu Mahavishnu Swami Thirukkovil</b> is located in Kaniyur, Madathukkulam division, Thirupur District. It is one of the few temples in India where you have both Sri Mahavishnu and Sri Mahalingam (Shiva/Siva) sitting side by side. If you are visiting Vetaikaranputhur or Kaniyur, this is one of the great temples to visit.

<b>Today's Inspirational Quote:</b>

<p class="tamil">
எது நடந்ததோ, அது நன்றாகவே நடந்தது.
எது நடக்கிறதோ, அது நன்றாகவே நடக்கிறது.
எது நடக்க இருக்கிறதோ,
அதுவும் நன்றாகவே நடக்கும்.
உன்னுடையதை எதை இழந்தாய்?
எதற்காக நீ அழுகிறாய்?
எதை நீ கொண்டு வந்தாய், அதை நீ இழப்பதற்கு?
எதை நீ படைத்திருந்தாய், அது வீணாவதற்கு?
எதை நீ எடுத்துக் கொண்டாயோ,
அது இங்கிருந்தே எடுக்கப்பட்டது,
எதை கொடுத்தாயோ,
அது இங்கேயே கொடுக்கப்பட்டது,
எது இன்று உன்னுடையதோ,
அது நாளை மற்றொருவருடையதாகிறது,
மற்றொரு நாள், அது வேறொருவருடையதாகும்<br/>
- பகவான் ஸ்ரீ கிருஷணர்
</p>

<p>
Whatever happened in the past, it happened for the good; Whatever is happening, is happening for the good; Whatever shall happen in the future, shall happen for the good only. Do not weep for the past, do not worry for the future, concentrate on your present life. What did you bring at the time of birth, that you have lost? What did you produce,  which is destroyed? You didn't bring anything when you were born. Whatever you have, you have received it from the God only while on this earth. Whatever you will give, you will give it to the God. Everyone came in this world empty handed and shall go the same way. Everything belongs to God only. Whatever belongs to you today, belonged to someone else earlier and shall belong to some one else in future. Change is the law of the universe.<br/>
- Lord Sri Krishna in Bhagavad Gita
</p>



